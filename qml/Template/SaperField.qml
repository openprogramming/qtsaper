import QtQuick 2.0

Rectangle {
    id: field
    width: 36
    height: 36

    color: "gray"

    signal mineDetonated()

    Text {
        id: label
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        text: "?"
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            field.color = fieldValue >= 0 ? "green" : "red"
            label.text = fieldValue

            if(fieldValue === -1)
                mineDetonated()
        }
    }
}

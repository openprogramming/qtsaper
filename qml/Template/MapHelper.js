function indexForCell(x, y) {
    return y * 10 + x;
}

function generateMap(listModel) {
    listModel.clear();

    for(var i = 0; i < 100; ++i)
        listModel.append({"fieldValue":0});

    for(var j = 0; j < 25; ++j)
    {
        var randomX = Math.floor(Math.random() * 9.99);
        var randomY = Math.floor(Math.random() * 9.99);

        var idx = MapHelper.indexForCell(randomX, randomY);
        console.log("Setting bomb for " + randomX + "x" + randomY +" | cellId: " + idx )

        var value = listModel.get(idx).fieldValue;
        if(value === -1)
            continue;

        listModel.setProperty(idx, "fieldValue", -1)
        MapHelper.updateValuesAboveBomb(randomX, randomY, listModel)
    }
}

function updateValuesAboveBomb(bombX, bombY, listModel)
{
    for(var x = bombX - 1; x <= bombX + 1; ++x)
    {
        for(var y = bombY - 1; y <= bombY + 1; ++y)
        {
            if(x >= 0 && x < 10 && y >= 0 && y < 10)
                incrementValueAtIndex(x, y, listModel)
        }
    }
}

function incrementValueAtIndex(x, y, listModel)
{
    var idx = indexForCell(x, y)
    var value = listModel.get(idx).fieldValue
    if(value !== -1)
    {
        console.log("increment " + idx + " of value " + value )
        listModel.setProperty(idx, "fieldValue", parseInt(value) + 1);
    }

}

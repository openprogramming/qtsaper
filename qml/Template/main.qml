import QtQuick 2.2
import "MapHelper.js" as MapHelper

Rectangle {
    width: 360
    height: 360

    Component.onCompleted: {
        MapHelper.generateMap(listModel)
    }

    Column{
        anchors.fill: parent
        Text {
            id: gameOverLabel;
            text: "GAME OVER"

            anchors.centerIn: parent
        }

        Rectangle {
            width: gameOverLabel.width
            height: 30
            anchors.top: gameOverLabel.bottom
            anchors.left: gameOverLabel.left

            color: "gray"

            Text {
                anchors.centerIn: parent
                text: "RESTART"
            }

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    MapHelper.generateMap(listModel)
                    grid.visible = true
                }
            }
        }

    }

    ListModel {
        id: listModel
    }

    GridView {
        id: grid

        width: 360
        height: 360

        cellWidth: 36
        cellHeight: 36

        model: listModel
        delegate: SaperField { onMineDetonated: grid.visible = false }
    }

}
